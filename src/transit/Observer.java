/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <lilyjlux@gmail.com, gonzalezn@msoe.edu, galluntf@msoe.edu> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy us a beer in return Lily Lux, Noe Gonzalez, Trey Gallun.
 * ----------------------------------------------------------------------------
 */

package transit;

/**
 * Display observers
 *
 * @author Lily Lux
 * @version 1.0
 * @created 05-Oct-2017 12:10:51 PM
 * <p>
 * The Observer to observer a Subject.
 */
public interface Observer {

    /**
     * Updates the observer.
     *
     * @param obj The subject it is attached to.
     * @author Lily Lux
     */
    public void update(Object obj);

}